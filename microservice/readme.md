Microservices for the Enterprise: Designing, Developing, and Deploying
https://learning.oreilly.com/library/view/microservices-for-the/9781484238585/

Case for Microservice
- Monolithic applications - characteristics
-- designed, developed, deployed as a single unit
-- complexity in maintaining, upgrading, adding new features
-- proprietary communication protocols, point to point communication styles
-- slower to start as the app grows, difficult to scale with conflicting resource requirements
-- single service brings down the entire app
-- difficult to adopt new technologies & frameworks

SOA & ESB
- service is a self-contained, well defined business functionality assessible over the network - SOA service
- services are software components with well-defined interfaces that are implementation independent
- consumers are only concerned with the service interface - separation of the what from the howA
- services are self contained, loosely coupled
- services can be discovered dynamically by it's metadata via a service metadata repository/registry
- composite services can be build by aggregating services
- a enterprise service bus is used to plumb/integrate services, data and system for consumption

<img src="461146_1_En_1_Fig2_HTML_esb.jpeg">

- ESB needs to create and expose functionality ( business capability via services or composite servics ) as well ascross cutting concerns - eg security, monitoring, analytics 
- ESB becomes the monolithic entity will all developers share runtime

APIs
- exposing business functionality as a managed service (ie API) is a key requirement
- using Web service/SOA is not ideal due to complexity in SOAP (inter services communication), WSDL (define service contract), WS-security (to secure messages)
- most organisation added a API Managament/Gateway layer (ie API Facade)
- also use it for security, throttling, caching, monentisation
- no longer able to cater with growing complexity

<img src="461146_1_En_1_Fig3_HTML.jpeg">

Microservice
- a single application build as a suite of small and independent services running their own processes, developed and deployed independently

<img src="461146_1_En_1_Fig4_HTML.jpeg">
- API gateway remains, but have the option of segregating into independent per API based runtime

Business oriented capability
- Each service is designed based on one of these principles
-- Single Responsibility Principle (SRP)
-- Conway's law
-- Twelve Factor App
-- Domain Drive design

Autonomous: Develop, Deploy, Scale independently
-- microservices are developed, deploy and scaled as independent entities
-- don't share common runtime, rather they leverage on technologies such as containers ( ie Docker, k8s, Meso )
-- inter-service communication can be built on top of various interaction styles and message format
-- independent services can scale up/down and with failures isolated at service level
-- service integration style is a new style call smart endpoints and dumb pipes. Business logic is encapsulated in the service endpoint. Services are connected using a primitive messaging system (ie dumb pipe) without business logic
-- microservice implements 
--- composition ( using sync and async styles)
--- interservice comms ( using different protocols
--- resiliency patterns ( eg circult breakers )
--- integrations with other apps and SaaS
--- observability

Fault Tolerance
-- resilency related capabilities - ie circult breaker, load balancing, fail over, dynamic scaling based on traffic pattern 
-- Chao Monkey - mimicking failures 
- solid observability structure to view latency, TPS

Decentralised data management
- choosing of separate data management techniques

Service Governance
- decentralised process where each team/entity is allowed to govern its own domain
- design time governance ( selecting technology, protocol, code review )
- runtime governance ( service definition, registry, discovery, versioning, dependencies, ownership and consumers, QoS, observerability)

Observability
- logging, distributed tracking, visualisation of service's runtime behaviour & dependencies
- use during development - debugging, potential issues
- use during production - monetization

Microservice Benefits
- agile / rapid development of business functionalities
- replaceability
- failure isolation and predictability
- agile development and scaling
- align with organisation structure

Microservice liabilities
- Inter-service communication ( complexity ). Creating composite business functionality requires plumbing microservices together which takes considerable time
- Service governance
- Heavily depends on Deployment Methodologies ( ie adoption of containers and container orchestration systems )
- Complexity of distributed data and transaction management

How and When to Use
- When enterprise requires modularity
- Software application has to embrace container-based deployment
- start by implementing a small use case
- each domain ( data mgmt, inter service comms, security ) has it's own best practice

2 Designing Microservice

Domain Driven design
- Domain refers to the main business subject area 
- Conway's Law - the design should produce a structure that is copy of the organisation communication structure
- ubitiquous language
- bounded context -> scoping a microservice

Typical SOA architecture
<img src="461146_1_En_2_Fig3_HTML.jpeg">

- Any domain consist of multiple bound contexts and each bounded context encapsulate it's functionalities into domain models and integrations to other bounded context ( ie explicit interfaces )

Multiple microservice under Inventory and Order Management domain, representing each of the bounded context
- order processing
- inventory
- supplier management
Here the service boundaries are aligned to the bounded contexts of the corresponding domain. The communication between bounded contexts happens only via message passing against well-defined interfaces. As per Figure 2-4, the Order Processing microservice first updates the Inventory microservice to lock the items in the order and then triggers the ORDER_PROCESSING_COMPLETED event. The Billing microservice listening to the ORDER_PROCESSING_COMPLETED event executes payment processing and then triggers the PAYMENT_PROCESSING_COMPLETED event. The Supplier Management microservice listening to the PAYMENT_PROCESSING_COMPLETED event checks whether the number of items in the stocks is above the minimal threshold and if not notifies the suppliers. The Delivery microservice listening to the same event executes its operations to look up the items (probably sending instructions to warehouse bots) and then groups the items together to build the order and make it ready for delivery. Once that’s done, the Delivery microservice will trigger the ORDER_DISPATCHED event, which notifies the Order Processing microservice, and will update the order status.
<img src="461146_1_En_2_Fig4_HTML.jpeg">

Domain Events
- communication between microservices can happen via events - aka domain events
- events are triggered when the state of bounded context changes
- domain events can be used between bounded context within a domain or between domains

Context Map
- visualize the relation between bounded context as a organisation grows

Vaughn Vernon, in his book Implementing Domain-Driven Design, presents multiple approaches to express a context map. The easier way is to come up with a diagram to show the mapping between two or more existing bounded contexts, as in Figure 2-5. Also, keep in mind here that each bounded context in Figure 2-5 has a corresponding microservice. We used a line between two bounded contexts with two identifiers at each end, either U or D, to show the relationship between corresponding bounded contexts. U is for upstream, while D is for downstream.

<img src="461146_1_En_2_Fig5_HTML.jpeg">

- upstream bounded context defines the structure of the event, interested downstream bounded context must be compliant

Anti-corruption layer
- When Order Processing updates the Inventory, it has to translate its own order entity to the order entity, which is understood by the Inventory bounded context. We use the anti-corruption layer (ACL) pattern to address this concern. The anti-corruption layer pattern provides a translation layer

<img src="461146_1_En_2_Fig6_HTML.jpeg">

There are multiple ways to implement the anti-corruption layer. One approach is to build the translation into the microservice itself. You will be using whatever the language you used to implement the microservice to implement the anti-corruption layer as well. This approach has multiple drawbacks. The microservice development team has to own the implementation of the anti-corruption layer; hence they need to worry about any changes happening at the monolithic application side. If we implement this translation layer as another microservice, then we can delegate its implementation and the ownership to a different team. That team only needs to understand the translation and nothing else. This approach is commonly known as the sidecar pattern.

The communication between the microservice and the sidecar happens over the network (not a local, in-process call), but both the microservice and sidecar are co-located in the same host—so it will not be routed over the network
<img src="461146_1_En_2_Fig8_HTML.jpeg">
