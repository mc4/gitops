https://faun.pub/basic-ci-cd-for-python-projects-with-docker-and-jenkins-38eeb547fb28

https://learning.oreilly.com/library/view/test-driven-development-with/9781491958698/preface02.html#idm140633381691168
Test-Driven Development with Python, 2nd Edition
https://github.com/hjwp/book-example/tree/master

chap2:
Functional Test to Scope Out a Minimum Viable App

User Story
A description of how the application will work from the point of view of the user. Used to structure a functional test.

Expected failure
When a test fails in the way that we expected it to.

chap3:
Unit Tests, and How They Differ from Functional Tests

1. We start by writing a functional test, describing the new functionality from the user’s point of view.
2. Once we have a functional test that fails, we start to think about how to write code that can get it to pass (or at least to get past its current failure). We now use one or more unit tests to define how we want our code to behave—the idea is that each line of production code we write should be tested by (at least) one of our unit tests.
3. Once we have a failing unit test, we write the smallest amount of application code we can, just enough to get the unit test to pass. We may iterate between steps 2 and 3 a few times, until we think the functional test will get a little further.
4. Now we can rerun our functional tests and see if they pass, or get a little further. That may prompt us to write some new unit tests, and some new code, and so on.

Functional tests should help you build an application with the right functionality, and guarantee you never accidentally break it. 
Unit tests should help you to write code that’s clean and bug free.

We’ve now seen all the main aspects of the TDD process, in practice:

- Functional tests
- Unit tests
- The unit-test/code cycle
- Refactoring

<img src="twp2_0404.png">


RED/GREEN/REFACTOR AND TRIANGULATION
- The unit-test/code cycle is sometimes taught as Red, Green, Refactor:

- Start by writing a unit test which fails (Red).
- Write the simplest possible code to get it to pass (Green), even if that means cheating.
- Refactor to get to better code that makes more sense.

TERMINOLOGY 2: UNIT TESTS VERSUS INTEGRATED TESTS, AND THE DATABASE
- Purists will tell you that a “real” unit test should never touch the database, and that the test I’ve just written should be more properly called an integrated test, because it doesn’t only test our code, but also relies on an external system—that is, a database.

chap5:

Setup, Exercise, Assert is the typical structure for a unit test.

USEFUL TDD CONCEPTS
- Regression
When new code breaks some aspect of the application which used to work.

- Unexpected failure
When a test fails in a way we weren’t expecting. This either means that we’ve made a mistake in our tests, or that the tests have helped us find a regression, and we need to fix something in our code.

- Red/Green/Refactor
Another way of describing the TDD process. Write a test and see it fail (Red), write some code to get it to pass (Green), then Refactor to improve the implementation.

- Triangulation
Adding a test case with a new specific example for some existing code, to justify generalising the implementation (which may be a “cheat” until that point).

- Three strikes and refactor
A rule of thumb for when to remove duplication from code. When two pieces of code look very similar, it often pays to wait until you see a third use case, so that you’re more sure about what part of the code really is the common, re-usable part to refactor out.

- The scratchpad to-do list
A place to write down things that occur to us as we’re coding, so that we can finish up what we’re doing and come back to them later.
