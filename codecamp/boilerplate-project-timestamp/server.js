// server.js
// where your node app starts

// init project
var express = require('express');
var app = express();
var moment = require('moment')

// enable CORS (https://en.wikipedia.org/wiki/Cross-origin_resource_sharing)
// so that your API is remotely testable by FCC 
var cors = require('cors');
app.use(cors({optionsSuccessStatus: 200}));  // some legacy browsers choke on 204

// http://expressjs.com/en/starter/static-files.html
app.use(express.static('public'));

// http://expressjs.com/en/starter/basic-routing.html
app.get("/", function (req, res) {
  res.sendFile(__dirname + '/views/index.html');
});


// your first API endpoint... 
app.get("/api/hello", function (req, res) {
  res.json({greeting: 'hello API'});
});

app.get("/api/:date", function (req, res){

  var inputDate = req.params.date;
  var iDate = new Date(inputDate);
  console.log(inputDate);
  console.log(iDate);

  if (iDate.toString() != "Invalid Date") {

    console.log("Date is valid");
    unixTimestamp = parseInt((iDate.getTime()).toFixed(0))
    res.json({"unix": unixTimestamp, "utc": iDate.toUTCString()});

  }
  else {

    console.log("Date is Not valid");
    console.log(inputDate + " is Invalid");
    
  
    if (inputDate.toString().match(/^[0-9]+$/) != null ) {

      // check for unix timestamp
      const unixTimestamp = moment(parseInt(inputDate));


      console.log("Converted "+ inputDate + " successfully" );

      res.json({"unix": parseInt(inputDate), "utc": unixTimestamp.toDate().toUTCString()});


    }

    else {
        console.log(inputDate + " is Invalid");
        res.json({"error": "Invalid Date"});
    }



  }


})


app.get("/api/", function (req, res){

  var nowDate = new Date();
  var nowUnixTimestamp = parseInt(nowDate.getTime());

  res.json({"unix": nowUnixTimestamp, "utc": nowDate.toUTCString()});
})

// listen for requests :)
var listener = app.listen(process.env.PORT, function () {
  console.log('Your app is listening on port ' + listener.address().port);
});
