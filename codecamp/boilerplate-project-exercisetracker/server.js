const express = require('express')
const app = express()
const cors = require('cors')
require('dotenv').config()

var mongoose = require('mongoose');
var bodyParser = require('body-parser');

// mongoDB atlas instance
const mySecret = process.env['MONGO_URI'];
mongoose.connect(mySecret, { useNewUrlParser: true});

app.use(bodyParser.urlencoded({ extended: "false" }));
app.use(bodyParser.json());

app.use(cors())
app.use(express.static('public'))
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/views/index.html')
});

// Models
const { Schema } = mongoose;

const exerciseSchema = new Schema({
    // username:  String, // String is shorthand for {type: String}
    description: String,
    duration: Number,
    //date: Date
    date: String
  });

const userSchema = new Schema({
    username:  String // String is shorthand for {type: String}
  });

const logSchema = new Schema({
    username:  String, // String is shorthand for {type: String}
    count: Number,
    log: [
      exerciseSchema
    ]
  });

Exercise = mongoose.model('exercise', exerciseSchema);
User = mongoose.model('user', userSchema);
Log = mongoose.model('log', logSchema);

app.post("/api/users", function (req, res){

  var username = req.body.username;
  let u = new User({ "username": username });
  u.save(function (err, nUser) {
    if (err) {
      return next(err);
    }
  res.json(nUser);
  })
});

app.get("/api/users", function (req, res){

  User.find({})
  .exec(function (err, userList) {
    if (err) return console.log(err);

    console.log("Check userList array " + Array.isArray(userList));
    res.json(userList);
  });

});

app.post("/api/users/:_id/exercises", function (req, res){

  var _id = req.params._id;
  var description = req.body.description;
  var duration = req.body.duration;
  var date = req.body.date;

  console.log("param "+_id + "|" + description + "|" + duration + "|" + date);

  var iDate = new Date(date);
  if (iDate.toString() != "Invalid Date") {

    console.log('use date ' + iDate.toString());
  }
  else {
    var iDate = new Date();
    console.log('use default current date ' + iDate.toString());
  }

  User.find({ "_id" : _id })
  .exec(function (err, userList) {
    if (err) {
      console.log('_id not found '+ _id);
      return res.json(err);
    }

    foundUser = userList[0];
    if (foundUser === undefined) {
      console.log("User _id not found "+ _id);
      res.json({});
    }
    else {

      
      
      console.log(foundUser);
      //res.json(foundUser);

      let e = new Exercise({ 
        // "username": foundUser.username,
        "description": description,
        "duration": parseInt(duration),
        "date": iDate.toDateString()

      });

      e.save(function (err, nExercise) {
        if (err) {
          return res.json(err);
        }
      // create new Log object

        Log.find({"username": foundUser.username})
        .exec(function (err, logFound) {
          if (err) {
            console.log(err);
            res.json(err);
          }
          console.log('Found ');
          if (logFound.length == 0) {
            // create a new Log
            let l = new Log({ 
              "username": foundUser.username,
              "count": 1,
              "log": [nExercise]
            });
            console.log('First exercise');
            console.log(l);
            l.save(function (err, nLog) {
              if (err) {
                console.log(err);
                return res.json({});
              }
              //res.json(nLog);

            });

          }
          else {

            console.log('Found Log for exercise');
            console.log(logFound[0]);

            logFound[0].count = logFound[0].count + 1;
            logFound[0].log.push(nExercise);
            logFound[0].save(function (err, data) {
              if (err) return console.error(err);
              
              console.log('Updated Log with new Exercise');
              console.log(data);
              //res.json(data);
            });


            // res.json(logFound[0]);
          }
          
          let retJson = {
              "_id" : foundUser._id,
              "username" : foundUser.username,
              "date": nExercise.date,
              "duration" : nExercise.duration,
              "description" : nExercise.description
          };
          console.log('returning ');
          console.log(retJson);
          res.json(retJson);

        });

      }); // e.save()

    }; // foundUser

  }); // User.find().exec()



}); // app.post()

app.get("/api/users/:_id/logs", function (req, res){

  var _id = req.params._id;
  console.log(_id);

  var _from = req.query.from;
  var _to = req.query.to;
  var _limit = req.query.limit;

  console.log("param "+_id + "|" + _from + "|" + _to + "|" + _limit);

  User.findById( _id, function (err, userFound) {
    if (err) {
      console.log(err);
      res.json(err);
    }
    console.log(userFound); 
    if (userFound === undefined) {
      console.log("User _id not found "+ _id);
      // res.json({});
    }
    else {

      //Log.find({ "log.username" : userFound.username })
      Log.find({ "username" : userFound.username })
      .exec(function (err, logList) {
        if (err) {
          //console.log('_id , log.username not found '+ _id + ", " + 
          console.log('_id , username not found '+ _id + ", " + userFound.username);
          return res.json(err);
        }
        if (logList.length == 0){
          console.log(logList[0]);
          res.json({});
        }
        else {
          console.log(logList[0]);

          let sLogList = [];

          if (_from)
            fromDate = new Date(_from);
          else
            fromDate = new Date("1900-01-01");

          if (_to)
            toDate = new Date(_to);
          else
            toDate = new Date();
          

          console.log('Checking from ' + fromDate + ' to ' + toDate);

          for (log of logList[0].log) {

            cDate = new Date(log.date);

            if ( cDate.getTime() > fromDate.getTime()  &&
              ( toDate.getTime() >= cDate.getTime() ) )
            {
              console.log("date " + log.date);
              sLogList.push({
                "description" : log.description,
                "duration" : log.duration,
                "date" : log.date
              })
            }
            
          };

          if (_limit) 
            sLogList = sLogList.slice(0, parseInt(_limit)); 

          let rLogObj = {
            "_id": _id,
            "username": userFound.username,
            "from": fromDate.toDateString(),
            "to": toDate.toDateString(),
            "count": sLogList.length,
            "log": sLogList
          }
          console.log(rLogObj);
          res.json(rLogObj);
          // res.json(logList[0]);
        }
        
      });

    } // userFound

  });

});


const listener = app.listen(process.env.PORT || 3000, () => {
  console.log('Your app is listening on port ' + listener.address().port)
})
