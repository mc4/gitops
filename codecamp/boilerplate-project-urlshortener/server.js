require('dotenv').config();
const express = require('express');
const cors = require('cors');
const app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var validUrl = require('valid-url');
const { URL, parse } = require('url');

// Basic Configuration
const port = process.env.PORT || 3000;

// creating model
const { Schema } = mongoose;
const urlSchema = new Schema({
    original_url:  String, // String is shorthand for {type: String}
    short_url: { type: Number, default: 1 }
  });

UrlShortner = mongoose.model('urlShortner', urlSchema);

const initTest = (done) => {

  const urlShortner = new UrlShortner({
    "original_url": "http://www.auguried.com"
    });

  urlShortner.save(function (err, data) {
      if (err) return console.error(err);
      console.log("urlShortner saved");
      console.log(data);
      done(null, data);
  }); 
  
};


app.use(cors());

app.use('/public', express.static(`${process.cwd()}/public`));

// mongoDB atlas instance
const mySecret = process.env['MONGO_URI'];
mongoose.connect(mySecret, { useNewUrlParser: true, useUnifiedTopology: true });

const TIMEOUT = 10000;

app.use(bodyParser.urlencoded({ extended: "false" }));
app.use(bodyParser.json());


app.get('/', function(req, res) {
  res.sendFile(process.cwd() + '/views/index.html');
});

// Your first API endpoint
app.get('/api/hello', function(req, res) {
  res.json({ greeting: 'hello API' });
});

app.get("/api/init", function (req, res, next) {
  initTest(function (err, data) {
    if (err) {
      return next(err);
    }
    if (!data) {
      console.log("Missing `done()` argument");
      return next({ message: "Missing callback argument" });
    }
    res.json(data);
    next();
  });
});



function isValidHttpUrl(string) {
  let url;
  
  try {
    url = new URL(string);
  } catch (_) {
    return false;  
  }

  return url.protocol === "http:" || url.protocol === "https:";
}

app.post("/api/shorturl", function (req, res, next) {
  let t = setTimeout(() => {
    next({ message: "timeout" });
  }, TIMEOUT);

  console.log('shorturl ..');
  console.log(req.body);
  

  UrlShortner.find({})
  .sort({short_url: -1})
  .limit(1)
  .exec(function (err, urlFound) {
    if (err) return console.log(err);
    console.log(urlFound);
    
    oUrl = req.body.url;
    console.log("New Post " + oUrl );

    if (isValidHttpUrl(oUrl)){
      
      if (urlFound.length == 0)
        newShort_url = 1;
      else
        newShort_url = parseInt(urlFound[0].short_url) + 1;
      console.log("Saving new urlShortner ... " + oUrl + " " + newShort_url);

      const newUrlShortner = new UrlShortner({
        "original_url": oUrl,
        "short_url": newShort_url
      });
      newUrlShortner.save(function (err, data) {
        if (err) return console.error(err);
        retJson = { "original_url": data.original_url, "short_url": data.short_url};
        res.json(retJson);
      });
    }
    else {
      
      retJson = { "error": 'invalid url'};
      console.log(retJson);
      res.json(retJson);
    };

  });

  
});

app.get("/api/shorturl/:short_url", function (req, res){

  var short_url = req.params.short_url;
  console.log('Finding ... '+ short_url);

  UrlShortner.find({"short_url" : short_url})
  .sort({short_url: -1})
  .limit(1)
  .exec(function (err, urlFound) {
    if (err) {
      console.log(err);
      retJson = { "error": 'invalid url'};
      res.json(retJson);
    }
    console.log('Found ');
    console.log(urlFound[0]);
    if (urlFound[0] === undefined) {
      console.log(err);
      retJson = { "error": 'invalid url'};
      res.json(retJson);
    }

    else
  
      res.redirect(301, urlFound[0].original_url);

  })

});


app.listen(port, function() {
  console.log(`Listening on port ${port}`);
});
