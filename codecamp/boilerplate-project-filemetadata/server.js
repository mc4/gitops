var express = require('express');
var cors = require('cors');
require('dotenv').config();

const bodyParser= require('body-parser');
const multer = require('multer');

var app = express();
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());

// SET STORAGE
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now())
  }
})
 
var upload = multer({ storage: storage })

app.use('/public', express.static(process.cwd() + '/public'));

app.get('/', function (req, res) {
    res.sendFile(process.cwd() + '/views/index.html');
});


app.post('/api/fileanalyse', upload.single('upfile'), (req, res, next) => {
  const file = req.file
  if (!file) {
    const error = new Error('Please upload a file')
    error.httpStatusCode = 400
    console.log(error);
    res.json('Error encountered')
    
  }
    console.log('Uploaded');
    console.log(file);
    let retJson = {
      "name": file.originalname,
      "type": file.mimetype,
      "size": file.size
    };
    //res.send(file)
    res.json(retJson);
  
})

const port = process.env.PORT || 3000;
app.listen(port, function () {
  console.log('Your app is listening on port ' + port)
});
