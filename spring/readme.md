Learn Spring Core Framework the Easy Way
queue
By Karthikeya T

https://learning.oreilly.com/videos/learn-spring-core/9781801071680/

reference:
https://github.com/PacktPublishing/Learn-Spring-Core-Framework-the-Easy-Way-

- Intro 
- Setting up environment
- Dependency Injection
- IoC Container
- Constructor and Setter Injection
- Bean Life Cycle
-- Init Destroy
-- Bean Post Processor
-- Spring Wiring
- Aware Interfaces
--Theory
--Bean Aware
--Resource Loader Aware
--Application context aware

- Singleton Design
Spring Beans
- no state, singelton, eagerly loaded using the constructor
- eg database bean is typically eager loading

- Prototype Scope
-- when u need Beans with state in Spring

- Autowiring

- Property editors ( customising the types of bean's instantiation process which are to be injected instantiation iinto the Spring containers )
-- profile annotation in the code
-- using vm arguments

- Profiles ( eg environment configurations )

- Factory Beans ( controlling Beans instantiation process using methods/logic - instead of configuration profiles)
-- eg checking apikey's validitiy

Aspect oriented programming
- cross cutting concerns
eg a loan, credit card, admin service needs to address cross cutting concerns like
-- security, transaction, logging ( aspects )

- AOP theory
-- use of a Proxy object ( ie Aspect classes & methods - ie MethodBeforeAdvice )
-- joint pointsi ( ie methods where the aspect advice methods needs to be run) 
--  weaving ( ie associating the aspect methods to the joint points )
-- pointcut ( expression language to specify which classes the aspect advice will run ) 
-- using aspectj, weaving operations happens during compilation time, not runtime ( ie Spring AOP objects )

-- order of control
-- attributes of expression language, targetting what
