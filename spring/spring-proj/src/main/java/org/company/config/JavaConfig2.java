package org.company.config;

import org.company.Animal;
import org.company.AnimalNoise;
import org.company.Dog;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JavaConfig2 {
		
	
	@Bean
	public AnimalNoise animalNoise(Animal animal) {
		
		return new AnimalNoise(animal);
	}
	
	
}
