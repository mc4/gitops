package org.company.config;

import org.company.Animal;
import org.company.Cat;
// import org.company.AnimalNoise;
import org.company.Dog;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(JavaConfig2.class)
public class JavaConfig {
	
	@Bean
	public Animal animal() {
		
		// return new Dog();
		return new Cat();
	}
	
	
}
