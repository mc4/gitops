package org.company;

public class Cat implements Animal{

	public void makeNoise(String name) {
		System.out.println(name + " says Meow meow..");
	}

}
